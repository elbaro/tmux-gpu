`~/.tmux.conf`에 다음 추가

```
set -g @plugin 'tmux-plugins/tmux-gpu'
```

```
set -g status-right-length 120
set -g status-right 'CPU: #{cpu_bg_color}#{cpu_percentage}#[fg=#666666]#[bg=default] GPU: #{gpu_info}'
```


