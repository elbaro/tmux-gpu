package main

// #cgo CPPFLAGS: -I/usr/local/cuda-8.0/targets/x86_64-linux/include/
// #cgo LDFLAGS: /usr/local/cuda-8.0/targets/x86_64-linux/lib/stubs/libnvidia-ml.so
// #include <nvml.h>
import "C"

import (
	"bytes"
	"errors"
	"fmt"
	// "github.com/elabro/tmux-gpu" gpu
)

func processError(code C.nvmlReturn_t) {
	if code != 0 {
		msg := C.GoString(C.nvmlErrorString(code))
		panic(errors.New(msg))
	}
}

func handleByIndex(idx int) C.nvmlDevice_t {
	var device C.nvmlDevice_t
	processError(C.nvmlDeviceGetHandleByIndex(C.uint(idx), &device))
	return device
}

// 01234567
// ▁▂▃▄▅▆▇█
// ▁
func spark(rate float64) rune {
	level := int(rate * 8) // 0~0.999 -> 0~7,   1.00 -> 8
	if level == 8 {
		level = 7
	}

	return rune('▁' + level)
}

func main() {
	processError(C.nvmlInit())
	defer C.nvmlShutdown()

	var count C.uint = 0
	processError(C.nvmlDeviceGetCount(&count))
	deviceCount := int(count)

	var msgBuffer bytes.Buffer
	// sum := 0.0
	// cnt := 0

	for i := 0; i < deviceCount; i++ {
		handle := handleByIndex(i)

		var memoryInfo C.nvmlMemory_t
		err := C.nvmlDeviceGetMemoryInfo(handle, &memoryInfo)

		if err == 0 {
			used := float64(memoryInfo.used)
			total := float64(memoryInfo.total)
			load := used / total

			var color string
			if load*8 < 1 {
				color = "#[fg=green]"
			} else if load*8 < 7 {
				color = "#[fg=yellow]"
			} else {
				color = "#[fg=red]"
			}

			// txt := fmt.Sprintf("%s%d [%c]%4.1f%%  ", color, i, spark(load), load*100)
			// txt := fmt.Sprintf("%s[%d %c %4.1f%%]  ", color, i, spark(load), load*100)
			txt := fmt.Sprintf("%s[%d %c] ", color, i, spark(load))
			msgBuffer.WriteString(txt)
			// sum += load
			// cnt += 1
		} else {
			txt := fmt.Sprintf("%d[??]      ", i)
			msgBuffer.WriteString(txt)
		}
	}
	longMsg := msgBuffer.String()
	// shortMsg := fmt.Sprintf("%d[%q]%4.1f%%", )
	fmt.Println(longMsg)
}
